package id.alami.be.bealami.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "tb_data_transaction")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DataTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @Column(name="credit", columnDefinition = "INTEGER", nullable = false)
    private Integer credit;

    @Column(name="debit", columnDefinition = "INTEGER", nullable = false)
    private Integer debit;

    @Column(name="loan", columnDefinition = "INTEGER", nullable = false)
    private Integer loan;

    @Column(name="pay_loan", columnDefinition = "INTEGER", nullable = false)
    private Integer payLoan;

    @Column(name="transaction_date", columnDefinition = "DATE", nullable = false)
    private LocalDate transactionDate;
}
