package id.alami.be.bealami.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name="tb_user")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name", columnDefinition = "VARCHAR(150)", nullable = false)
    private String name;

    @Column(name="birthdate", columnDefinition = "DATE", nullable = false)
    private LocalDate birthDate;

    @Column(name="address", columnDefinition = "TEXT", nullable = false)
    private String address;
}
