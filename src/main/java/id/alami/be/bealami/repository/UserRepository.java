package id.alami.be.bealami.repository;

import id.alami.be.bealami.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    User findById(Integer id);

    Optional<User> findAllByNameContains(String name);
}
