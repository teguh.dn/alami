package id.alami.be.bealami.repository;

import id.alami.be.bealami.model.DataTransaction;
import id.alami.be.bealami.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface DataTransactionRepository extends CrudRepository<DataTransaction, Long> {
    List<DataTransaction> findAllByUser(User user);

    @Query(value = "from DataTransaction t where t.transactionDate BETWEEN :startDate AND :endDate")
    List<DataTransaction> getAllBetweenDates(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate);
}
