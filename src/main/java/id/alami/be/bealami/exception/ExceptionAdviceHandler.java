package id.alami.be.bealami.exception;

import com.google.common.base.CaseFormat;
import id.alami.be.bealami.response.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionAdviceHandler {


    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<ApiResponse> handleJXRequestException(RecordNotFoundException ex){
        ApiResponse apiResponse = new ApiResponse(false , ex.getCode() , null, ex.getMessage());
        return new ResponseEntity<>(apiResponse, ex.getHttpStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationExceptions(MethodArgumentNotValidException ex){
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) ->{
            String fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE , ((FieldError) error).getField());
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName,errorMessage);
        });
        return ResponseEntity.badRequest().body(ApiResponse.badRequest(errors));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<?> handleTypeMismatchExceptions(MethodArgumentTypeMismatchException ex) {
        Map<String, String> errors = new HashMap<>();
        String field = ex.getParameter().getParameterName();
        String error = "Invalid Format Type";

        errors.put(field, error);

        return ResponseEntity.badRequest().body(ApiResponse.badRequest(errors));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<?> handleMissingServletRequestExceptions(MissingServletRequestParameterException ex) {
        Map<String, String> errors = new HashMap<>();
        String field = ex.getParameterName();
        String error = field+" parameter is missing .";

        errors.put("error", error);

        return ResponseEntity.badRequest().body(ApiResponse.badRequest(errors));
    }
}
