package id.alami.be.bealami.mapper;

import id.alami.be.bealami.model.User;
import id.alami.be.bealami.request.CreateUserRequest;
import id.alami.be.bealami.response.UserResponse;
import org.mapstruct.*;


@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    public abstract UserResponse convertToResponse(User user);

    @BeanMapping(qualifiedByName = "create")
    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    })
    public abstract User toModel(@MappingTarget User user, CreateUserRequest request);

    @Mappings({
            @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    })
    public abstract User toModelValue(@MappingTarget User user, UserResponse response);

}
