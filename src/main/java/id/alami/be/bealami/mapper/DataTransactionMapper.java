package id.alami.be.bealami.mapper;

import id.alami.be.bealami.model.DataTransaction;
import id.alami.be.bealami.request.CreateUserRequest;
import id.alami.be.bealami.response.DataTransactionResponse;
import id.alami.be.bealami.response.UserResponse;
import org.mapstruct.*;


@Mapper(componentModel = "spring")
public abstract class DataTransactionMapper {
    @Mappings({
            @Mapping(source = "transactionDate", target = "transactionDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(target = "userId", source = "user.id")
    })
    public abstract DataTransactionResponse convertToResponse(DataTransaction dataTransaction);
}
