package id.alami.be.bealami.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateTransactionRequest {

    @NotNull
    private Integer userId;


    /*
        Transaction Type
        1 =  credit
        2 = debit
        3 = loan
        4 = pay_loan
    */
    @NotNull
    private Integer transactionType;

    @NotNull
    private Integer amount;
}
