package id.alami.be.bealami;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BealamiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BealamiApplication.class, args);
	}

}
