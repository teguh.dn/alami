package id.alami.be.bealami.controller;

import id.alami.be.bealami.exception.RecordNotFoundException;
import id.alami.be.bealami.model.User;
import id.alami.be.bealami.request.CreateUserRequest;
import id.alami.be.bealami.response.ApiResponse;
import id.alami.be.bealami.response.UserResponse;
import id.alami.be.bealami.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    UserService userService;

    @GetMapping("/list")
    public ApiResponse<?> getAllUser() throws RecordNotFoundException {
        List<UserResponse> listUser = new ArrayList<>();

        listUser = userService.getAllUser();

        return ApiResponse.success(listUser);
    }

    @GetMapping("/detail")
    public ApiResponse<?> getUserById(@RequestParam Integer id ) throws RecordNotFoundException{
        UserResponse userResponse = new UserResponse();
        userResponse =  userService.getUserById(id);
        return ApiResponse.success(userResponse);
    }

    @PostMapping("/create")
    public ApiResponse<?> createCompany(@RequestBody @Valid CreateUserRequest createUserRequest) throws RecordNotFoundException {

        UserResponse response = userService.create(createUserRequest);
        return ApiResponse.success(response);
    }
}
