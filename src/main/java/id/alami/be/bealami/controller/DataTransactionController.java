package id.alami.be.bealami.controller;

import id.alami.be.bealami.exception.RecordNotFoundException;
import id.alami.be.bealami.request.CreateTransactionRequest;
import id.alami.be.bealami.response.ApiResponse;
import id.alami.be.bealami.response.DataTransactionResponse;
import id.alami.be.bealami.service.DataTransactionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/transaction")
public class DataTransactionController {
    DataTransactionService dataTransactionService;

    @PostMapping("/create")
    public ApiResponse<?> createTransaction(@RequestBody @Valid CreateTransactionRequest createTransactionRequest) throws RecordNotFoundException {

        DataTransactionResponse response = dataTransactionService.create(createTransactionRequest);
        return ApiResponse.success(response);
    }

    @GetMapping("/list")
    public ApiResponse<?> getTransaction(
            @RequestParam Integer userId
            ) throws RecordNotFoundException{
        List<DataTransactionResponse> response = new ArrayList<>();
        response =  dataTransactionService.getListTransactionByUserId(userId);
        return ApiResponse.success(response);
    }

    @GetMapping("/list-transaction")
    public ApiResponse<?> getTransactionByDate(
            @RequestParam String startDate,
            @RequestParam String endDate
    ) throws RecordNotFoundException{
        List<DataTransactionResponse> response = new ArrayList<>();
        response =  dataTransactionService.getListTransactionByDate(startDate, endDate);
        return ApiResponse.success(response);
    }
}
