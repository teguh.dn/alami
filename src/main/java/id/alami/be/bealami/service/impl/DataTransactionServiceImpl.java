package id.alami.be.bealami.service.impl;

import id.alami.be.bealami.enums.ResponseCode;
import id.alami.be.bealami.enums.ResponseMessage;
import id.alami.be.bealami.exception.RecordNotFoundException;
import id.alami.be.bealami.mapper.DataTransactionMapper;
import id.alami.be.bealami.mapper.UserMapper;
import id.alami.be.bealami.model.DataTransaction;
import id.alami.be.bealami.model.User;
import id.alami.be.bealami.repository.DataTransactionRepository;
import id.alami.be.bealami.request.CreateTransactionRequest;
import id.alami.be.bealami.response.DataTransactionResponse;
import id.alami.be.bealami.response.UserResponse;
import id.alami.be.bealami.service.DataTransactionService;
import id.alami.be.bealami.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class DataTransactionServiceImpl implements DataTransactionService {

    private final UserService userService;
    private final DataTransactionRepository dataTransactionRepository;
    private final UserMapper userMapper;
    private final DataTransactionMapper dataTransactionMapper;

    @Override
    public DataTransactionResponse create(CreateTransactionRequest createTransactionRequest) throws RecordNotFoundException {
        DataTransactionResponse response = new DataTransactionResponse();

        DataTransaction data =  new DataTransaction();
        LocalDate today = LocalDate.now();
        if(createTransactionRequest.getAmount() < 1)throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "Amount must be more than 1");

        try {
            UserResponse userResponse = userService.getUserById(createTransactionRequest.getUserId());
            User user =     new User();
            userMapper.toModelValue(user, userResponse);
            data.setUser(user);
            if(createTransactionRequest.getTransactionType().equals(1)){
                data.setCredit(createTransactionRequest.getAmount());
                data.setDebit(0);
                data.setLoan(0);
                data.setPayLoan(0);
            }else if(createTransactionRequest.getTransactionType().equals(2)){
                data.setCredit(0);
                data.setDebit(createTransactionRequest.getAmount());
                data.setLoan(0);
                data.setPayLoan(0);
            }else if(createTransactionRequest.getTransactionType().equals(3)){
                data.setCredit(0);
                data.setDebit(0);
                data.setLoan(createTransactionRequest.getAmount());
                data.setPayLoan(0);
            }
            else if(createTransactionRequest.getTransactionType().equals(4)){
                data.setCredit(0);
                data.setDebit(0);
                data.setLoan(0);
                data.setPayLoan(createTransactionRequest.getAmount());
            }
            data.setTransactionDate(today);

            data = dataTransactionRepository.save(data);

            response = dataTransactionMapper.convertToResponse(data);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }

        return response;
    }

    @Override
    public List<DataTransactionResponse> getListTransactionByUserId(Integer userId) throws RecordNotFoundException{
        List<DataTransactionResponse> responses = new ArrayList<>();

        try {
            UserResponse userResponse = userService.getUserById(userId);
            User user =     new User();
            userMapper.toModelValue(user, userResponse);

            List<DataTransaction> data = dataTransactionRepository.findAllByUser(user);

            for (DataTransaction dataTransaction : data){
                responses.add(dataTransactionMapper.convertToResponse(dataTransaction));
            }

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return responses;
    }

    @Override
    public List<DataTransactionResponse> getListTransactionByDate(String startDate, String endDate) throws RecordNotFoundException{
        List<DataTransactionResponse> responses = new ArrayList<>();

        try {
            LocalDate start = LocalDate.parse(startDate);
            LocalDate end = LocalDate.parse(endDate);
            List<DataTransaction> data = dataTransactionRepository.getAllBetweenDates(start, end);

            for (DataTransaction dataTransaction : data){
                responses.add(dataTransactionMapper.convertToResponse(dataTransaction));
            }

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return responses;
    }
}
