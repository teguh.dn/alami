package id.alami.be.bealami.service;

import id.alami.be.bealami.exception.RecordNotFoundException;
import id.alami.be.bealami.model.User;
import id.alami.be.bealami.request.CreateUserRequest;
import id.alami.be.bealami.response.UserResponse;

import java.util.List;

public interface UserService {

    List<UserResponse> getAllUser() throws RecordNotFoundException;;

    UserResponse getUserById(Integer id) throws RecordNotFoundException;

    UserResponse create(CreateUserRequest createUserRequest) throws RecordNotFoundException;


}
