package id.alami.be.bealami.service.impl;

import id.alami.be.bealami.enums.ResponseCode;
import id.alami.be.bealami.enums.ResponseMessage;
import id.alami.be.bealami.exception.RecordNotFoundException;
import id.alami.be.bealami.mapper.UserMapper;
import id.alami.be.bealami.model.User;
import id.alami.be.bealami.repository.UserRepository;
import id.alami.be.bealami.request.CreateUserRequest;
import id.alami.be.bealami.response.UserResponse;
import id.alami.be.bealami.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public List<UserResponse> getAllUser() throws RecordNotFoundException {
        List<UserResponse> listResult =  new ArrayList<>();
        try {
            Iterable<User> users = userRepository.findAll();
            for(User user : users){
                listResult.add(userMapper.convertToResponse(user));
            }
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return listResult;
    }

    @Override
    public UserResponse getUserById(Integer id) throws RecordNotFoundException{
        UserResponse userResponse = new UserResponse();

        try{
            User user = userRepository.findById(id);

            userResponse = userMapper.convertToResponse(user);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return userResponse;
    }

    @Override
    public UserResponse create(CreateUserRequest createUserRequest) throws RecordNotFoundException {
        User user = new User();
        UserResponse response = new UserResponse();
        userMapper.toModel(user, createUserRequest);

        LocalDate today = LocalDate.now();
        if(user.getBirthDate().isAfter(today)) throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "BirthDate can't be more than current date");
        if(createUserRequest.getName().length() < 3) throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "Name must be longer more than 2 Characters");
        try{
            user = userRepository.save(user);
            response = userMapper.convertToResponse(user);
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }
}
