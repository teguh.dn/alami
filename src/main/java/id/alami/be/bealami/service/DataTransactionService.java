package id.alami.be.bealami.service;

import id.alami.be.bealami.exception.RecordNotFoundException;
import id.alami.be.bealami.request.CreateTransactionRequest;
import id.alami.be.bealami.response.DataTransactionResponse;

import java.util.List;

public interface DataTransactionService {

    DataTransactionResponse create(CreateTransactionRequest createTransactionRequest) throws RecordNotFoundException;

    List<DataTransactionResponse> getListTransactionByUserId(Integer id) throws RecordNotFoundException;

    List<DataTransactionResponse> getListTransactionByDate(String startDate, String endDate) throws RecordNotFoundException;
}
