package id.alami.be.bealami.response;

import id.alami.be.bealami.model.User;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataTransactionResponse {
    private Integer id;
    private Integer userId;
    private Integer credit;
    private Integer debit;
    private Integer loan;
    private Integer payLoan;
    private String transactionDate;

}
