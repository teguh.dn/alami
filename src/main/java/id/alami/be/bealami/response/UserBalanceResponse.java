package id.alami.be.bealami.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserBalanceResponse {

    private UserResponse user;
    private List<DataTransactionResponse> listTransaction;
    private Integer totalBalance;
    private Integer totalLoan;
}
