package id.alami.be.bealami.response;

import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserResponse {
    private Integer id;
    private String name;
    private String birthDate;
    private String address;
}
