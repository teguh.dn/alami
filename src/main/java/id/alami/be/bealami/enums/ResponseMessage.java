package id.alami.be.bealami.enums;

public enum ResponseMessage {
	SUCCESS("Success"),
	DATA_NOT_COMPLETE("please fill all field"),
	EMAIL_VERIFICATION("email invalid or domain must be @j-express.id"),
	FAILED_TO_SAVE_DATA("failed to save data"),
	NOT_FOUND("data not found"),
	IF_EXIST("data is exist"),
	DUPLICATE_DATA("data duplicated, please check again"),
	STATUS_VERIFIED("bad request status"),
	ROLE_NOT_FOUND("role not found"),
	STATUS_NOT_FOUND("status not found"),
	FAILED_TO_DELETE("failed to delete data");


	public String getMessage() {
		return message;
	}

	private final String message;

	ResponseMessage(String message) {
		this.message = message;
	}
}