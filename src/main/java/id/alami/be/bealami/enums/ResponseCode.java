package id.alami.be.bealami.enums;

public enum ResponseCode {
	SUCCESS(200, "Success"),
	NO_CONTENT(204, "Success"),
	BAD_REQUEST(400, "Bad Request"),
	UNAUTHORIZED(401, "Unauthorized"),
	NOT_FOUND(404, "Not Found"),
	NOT_ACCEPTABLE(406,"Not Accept"),
	GENERAL_ERROR(500, "Error System, Please Contact Administrator");

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	private final int code;
	private final String message;

	ResponseCode(int code, String message) {
		this.code = code;
		this.message = message;
	}
}